﻿using System;

namespace Menu_with_math
{
    class Program
    {
        static void Main(string[] args)
        {
            bool exit = false;
            do
            {
                //Menu
                Console.WriteLine("Hello! c: Choose what you want to do");
                Console.WriteLine("\t1. Find the area of a square");
                Console.WriteLine("\t2. Find the sum of two numbers");
                Console.WriteLine("\t3. Exit");

                int input = int.Parse(Console.ReadLine());

                //Area of square
                if (input == 1)
                {
                    Console.Write("Type in the length:");
                    int x = int.Parse(Console.ReadLine());

                    Console.Write("Type in the width:");
                    int y = int.Parse(Console.ReadLine());
                    Console.WriteLine("The area of the sqare is " + FindAreaOfSquare(x, y));
                    Console.ReadLine();
                }
                //Sum of numbers
                else if (input == 2)
                {
                    Console.Write("Type in the first number:");
                    int x = int.Parse(Console.ReadLine());

                    Console.Write("Type in the second number:");
                    int y = int.Parse(Console.ReadLine());
                    Console.WriteLine("The sum of the two numbers is " + FindSumOfTwoNumbers(x, y));
                    Console.ReadLine();
                }
                //Exit
                else if (input == 3)
                {
                    exit = true;
                }
                else
                {
                    Console.WriteLine("U stupid!");
                }
                Console.Clear();
            } while (exit != true);
        }

        //Idk why they have to be static :i
        public static int FindAreaOfSquare(int x, int y)
        {
            int result = x * y;
            return result;
        }

        public static int FindSumOfTwoNumbers(int x, int y)
        {
            int result = x + y;
            return result;
        }
    }
}
